import requests
from .keys import PEXELS_API_KEY, OPEN_WEATHER_API_KEY


# write two functions, one that makes a request to use Pexels API & one to
# make a request to Open Weather API. then use those functions in views.

# Pexels API
def get_image(city, state):
    url = f"https://api.pexels.com/v1/search?query={city}+{state}"
    headers = {"Authorization": PEXELS_API_KEY}
    resp = requests.get(url, headers=headers)
    data = resp.json()
    return data["photos"][0]["src"]["original"]


# Weather App API (1) - translate city & state for conference into lat & lon
def get_coord(city, state):
    url = f"http://api.openweathermap.org/geo/1.0/direct?q={city},{state},US&limit=5&appid={OPEN_WEATHER_API_KEY}"
# Make a request to Pexels passing in the city & state
    resp = requests.get(url)
    return {
        "lat": resp.json()[0]["lat"],
        "lon": resp.json()[0]["lon"]
    }


# Weather App API (2) - gets current weather for lat & lon
def get_weather_data(lat, lon):
    url = f"https://api.openweathermap.org/data/2.5/weather?lat={lat}&lon={lon}&units=imperial&appid={OPEN_WEATHER_API_KEY}"
    resp = requests.get(url)
    return {
        "description": resp.json()["weather"][0]["description"],
        "temp": resp.json()["main"]["temp"]
    }
