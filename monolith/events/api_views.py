from django.http import JsonResponse
from .models import Conference, Location, State
from common.json import ModelEncoder
from django.views.decorators.http import require_http_methods
import json
from .acls import get_coord, get_weather_data, get_image


class LocationListEncoder(ModelEncoder):
    model = Location
    properties = ["name"]


class LocationDetailEncoder(ModelEncoder):
    model = Location
    properties = [
        "name",
        "city",
        "room_count",
        "created",
        "updated",
        "image_url",
    ]
# is used for foreign key State data

    def get_extra_data(self, o):
        return {"state": o.state.abbreviation}


class ConferenceDetailEncoder(ModelEncoder):
    model = Conference
    properties = [
        "name",
        "description",
        "max_presentations",
        "max_attendees",
        "starts",
        "ends",
        "created",
        "updated",
        "location",
    ]
# since location is a foreign key, it needs to refer back to location encoder
    encoders = {
        "location": LocationDetailEncoder(),
    }


class ConferenceListEncoder(ModelEncoder):
    model = Conference
    properties = ["name", "id"]


# GET A LOCATION'S DETAILS
@require_http_methods(["GET", "POST"])
def api_list_locations(request):
    if request.method == "GET":
        location = Location.objects.all()
        return JsonResponse(
            {"location": location},
            encoder=LocationListEncoder,
        )
    else:

        # CREATE A LOCATION
        content = json.loads(request.body)
        content["image_url"] = get_image(content["city"], content["state"])
        try:
            state = State.objects.get(abbreviation=content["state"])
            content["state"] = state
        except State.DoesNotExist:
            return JsonResponse(
                {"message": "Invalid state abbreviation"},
                status=400,
            )

        # Use the city and state's abbreviation in the content dictionary
        # to call the get_photo ACL function

        # Use the returned dictionary to update the content dictionary

        # Get the State object and put it in the content dict
        state = State.objects.get(abbreviation=content["state"])
        content["state"] = state

        location = Location.objects.create(**content)
        return JsonResponse(
            location,
            encoder=LocationListEncoder,
            safe=False,
        )


# DELETE A LOCATION
@require_http_methods(["DELETE", "GET", "PUT"])
def api_show_location(request, id):
    if request.method == "GET":
        location = Location.objects.get(id=id)
        return JsonResponse(
            location,
            encoder=LocationDetailEncoder,
            safe=False,
        )
    elif request.method == "DELETE":
        count, _ = Location.objects.filter(id=id).delete()
        return JsonResponse({"deleted": count > 0})
# UPDATE A LOCATION
    else:
        content = json.loads(request.body)
        # Get the Location object (foreign key) and put in the content dict
        try:
            state = State.objects.get(abbreviation=content["state"])
            content["state"] = state
        except State.DoesNotExist:
            return JsonResponse(
                {"message": "Invalid location id"},
                status=400,
            )

        locations = Location.objects.update(**content)
        return JsonResponse(
            locations,
            encoder=LocationDetailEncoder,
            safe=False,
        )


# GET A CONFERENCE'S DETAILS
@require_http_methods(["GET", "POST"])
def api_list_conferences(request):
    if request.method == "GET":
        conferences = Conference.objects.all()
        return JsonResponse(
            {"conferences": conferences},
            encoder=ConferenceListEncoder,
        )
    else:
        # CREATE A CONFERENCE
        content = json.loads(request.body)
        # Get the Location object (foreign key) and put in the content dict
        try:
            location = Location.objects.get(id=content["location"])
            content["location"] = location
        except Location.DoesNotExist:
            return JsonResponse(
                {"message": "Invalid location id"},
                status=400,
            )

        conference = Conference.objects.create(**content)
        return JsonResponse(
            conference,
            encoder=ConferenceListEncoder,
            safe=False,
        )


# DELETE A CONFERENCE
@require_http_methods(["DELETE", "GET", "PUT"])
def api_show_conference(request, id):
    if request.method == "GET":
        conference = Conference.objects.get(id=id)

    # Use the city and state abbreviation of the Conference's Location
    # to call the get_weather_data ACL function and get back a dictionary
    # that contains the weather data

    # coords needs to go before weather to get coords first
        coords = get_coord(conference.location.city, conference.location.state.name)
        weather = get_weather_data(coords["lat"], coords["lon"])

    # Include the weather data in the JsonResponse (see it in the dictionary?)
        return JsonResponse(
            {"conference": conference, "weather": weather},
            encoder=ConferenceDetailEncoder,
            safe=False,
        )
    elif request.method == "DELETE":
        count, _ = Conference.objects.filter(id=id).delete()
        return JsonResponse({"deleted": count > 0})
# UPDATE A CONFERENCE
    else:
        content = json.loads(request.body)
        # Get the Location object (foreign key) and put in the content dict
        try:
            location = Location.objects.get(id=content["location"])
            content["location"] = location
        except Location.DoesNotExist:
            return JsonResponse(
                {"message": "Invalid location id"},
                status=400,
            )

        Conference.objects.filter(id=id).update(**content)
        conference = Conference.objects.get(id=id)
        return JsonResponse(
            conference,
            encoder=ConferenceDetailEncoder,
            safe=False,
        )
